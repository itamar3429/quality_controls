export enum DataSetType {
  Population = "population",
  Sample = "sample",
}

export const calcMean = (arr: number[]): number =>
  arr.length !== 0 ? arr.reduce((a, b) => a + b) / arr.length : 0.0;

export const calcStDevPop = (pop: number[]): number => {
  if (pop.length === 0) return 0.0;
  const mean = calcMean(pop);
  return Math.sqrt(
    pop.map((x) => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / pop.length
  );
};

export const calcStDevSamp = (sample: number[]): number =>
  Math.sqrt(
    sample
      .map((x) => x - sample.reduce((a, b) => a + b) / sample.length)
      .map((x) => Math.pow(x, 2))
      .reduce((a, b) => a + b) /
      (sample.length - 1)
  );

export const calcStDev = (
  arr: number[],
  dataSetType: DataSetType
): number =>
  dataSetType === DataSetType.Population
    ? calcStDevPop(arr)
    : calcStDevSamp(arr);
