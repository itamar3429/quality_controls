import { DataControls, PercentControlGraphData } from "./DataControls";
import { precN, repeatElement, sum } from "./utils";

/**
 * A class for datasets where the sample size varies (P-Charts)
 */
export class PercentDataWithControls implements DataControls {
  defects: number[];
  populationSizes: number[];
  p: number;
  /**
   *
   * @constructor Constructor method for PercentDataWithControls
   * @param {number[]} defects - A list of the defect occurences (numerator)
   * @param {number[]} populationSizes - A list of the population sizes (denominator)
   */
  constructor(defects: number[], sampleSizes: number[]) {
    this.defects = defects;
    this.populationSizes = sampleSizes;
    this.p = precN(sum(defects) / sum(sampleSizes), 10);
  }

  calcPControls(p: number) {
    return (n: number): number => 3 * Math.sqrt((p * (1 - p)) / n);
  }

  getMeanArr(): number[] {
    return repeatElement(this.p * 100, this.defects.length);
  }

  getLowerBoundsArr(): number[] {
    const controls = this.calcPControls(this.p);
    return this.populationSizes.map(
      (sampleSize) => precN(this.p - controls(sampleSize), 10) * 100
    );
  }

  getUpperBoundsArr(): number[] {
    const controls = this.calcPControls(this.p);
    return this.populationSizes.map(
      (sampleSize) => precN(this.p + controls(sampleSize), 10) * 100
    );
  }

  getData(): PercentControlGraphData {
    return {
      defects: this.defects,
      populationSizes: this.populationSizes,
      percent: this.defects.map(
        (defect, i) => precN(defect / this.populationSizes[i], 10) * 100
      ),
      mean: this.getMeanArr(),
      upperBounds: this.getUpperBoundsArr(),
      lowerBounds: this.getLowerBoundsArr(),
    };
  }
}
