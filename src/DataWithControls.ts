import { ControlGraphData, DataControls } from "./DataControls";
import { DataSetType, repeatElement } from ".";
import { calcMean, calcStDev } from "./statistics";

export class DataWithControls implements DataControls {
  defects: number[];
  mean: number;
  control: number;
  dataSetType: DataSetType;

  constructor(ticks: number[], dsType: DataSetType = DataSetType.Population) {
    this.defects = ticks;
    this.mean = calcMean(this.defects);
    this.control = calcStDev(ticks, dsType) * 3;
    this.dataSetType = dsType;
  }

  getMeanArr(): number[] {
    return repeatElement(this.mean, this.defects.length);
  }
  getLowerBoundsArr(): number[] {
    return repeatElement(
      parseFloat((this.mean - this.control).toFixed(2)),
      this.defects.length
    );
  }
  getUpperBoundsArr(): number[] {
    return repeatElement(
      parseFloat((this.mean + this.control).toFixed(2)),
      this.defects.length
    );
  }

  getData(): ControlGraphData {
    return {
      defects: this.defects,
      mean: this.getMeanArr(),
      upperBounds: this.getUpperBoundsArr(),
      lowerBounds: this.getLowerBoundsArr(),
    };
  }
}
