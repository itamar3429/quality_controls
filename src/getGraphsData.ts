import {
  ControlGraphData,
  DataControls,
  PercentControlGraphData,
} from "./DataControls";

import { precN } from "./utils";

export const getGraphsData = (
  d: DataControls
): ControlGraphData | PercentControlGraphData => ({
  defects: d.defects,
  populationSizes: d.populationSizes ? d.populationSizes : [],
  percent: d.defects.map((defect, i) =>
    d.populationSizes && d.populationSizes[i]
      ? precN(defect / d.populationSizes[i], 10) * 100
      : defect
  ),
  mean: d.getMeanArr(),
  upperBounds: d.getUpperBoundsArr(),
  lowerBounds: d.getLowerBoundsArr(),
});
