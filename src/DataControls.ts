/**
 * An interface for datasets with with upper and lower control limits
 */
export interface DataControls {
  defects: number[];
  populationSizes?: number[];
  getMeanArr(): number[];
  getLowerBoundsArr(): number[];
  getUpperBoundsArr(): number[];
  getData(): ControlGraphData;
}

export interface ControlGraphData {
  defects: number[];
  mean: number[];
  upperBounds: number[];
  lowerBounds: number[];
}

export interface PercentControlGraphData extends ControlGraphData {
  populationSizes: number[];
  percent: number[];
}
