"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataWithControls = void 0;
const _1 = require(".");
const statistics_1 = require("./statistics");
class DataWithControls {
    constructor(ticks, dsType = _1.DataSetType.Population) {
        this.defects = ticks;
        this.mean = statistics_1.calcMean(this.defects);
        this.control = statistics_1.calcStDev(ticks, dsType) * 3;
        this.dataSetType = dsType;
    }
    getMeanArr() {
        return _1.repeatElement(this.mean, this.defects.length);
    }
    getLowerBoundsArr() {
        return _1.repeatElement(parseFloat((this.mean - this.control).toFixed(2)), this.defects.length);
    }
    getUpperBoundsArr() {
        return _1.repeatElement(parseFloat((this.mean + this.control).toFixed(2)), this.defects.length);
    }
    getData() {
        return {
            defects: this.defects,
            mean: this.getMeanArr(),
            upperBounds: this.getUpperBoundsArr(),
            lowerBounds: this.getLowerBoundsArr(),
        };
    }
}
exports.DataWithControls = DataWithControls;
//# sourceMappingURL=DataWithControls.js.map