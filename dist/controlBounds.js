"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calcPControlBounds = exports.makePControlLimitsFunc = exports.calcControlBounds = void 0;
const statistics_1 = require("./statistics");
const utils_1 = require("./utils");
const calcControlBounds = (arr) => {
    const control = statistics_1.calcStDevPop(arr) * 3;
    const mean = statistics_1.calcMean(arr);
    return [
        parseFloat((mean - control).toFixed(2)),
        parseFloat((mean + control).toFixed(2)),
    ];
};
exports.calcControlBounds = calcControlBounds;
const makePControlLimitsFunc = (p) => (n) => {
    const control = 3 * Math.sqrt((p * (1 - p)) / n);
    return {
        UCL: utils_1.precN(p + control, 10) * 100,
        CL: p,
        LCL: utils_1.precN(p - control, 10) * 100,
    };
};
exports.makePControlLimitsFunc = makePControlLimitsFunc;
const calcPControlBounds = (defects, sampleSizes) => {
    const defectSum = utils_1.sum(defects);
    const sampleSizeSum = utils_1.sum(sampleSizes);
    const p = utils_1.precN(defectSum / sampleSizeSum, 10);
    const makePCLs = exports.makePControlLimitsFunc(p);
    return sampleSizes.map((np) => makePCLs(np));
};
exports.calcPControlBounds = calcPControlBounds;
//# sourceMappingURL=controlBounds.js.map