"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatPercentage = exports.calcPercentageDecimal = exports.calcNumerator = void 0;
const calcNumerator = (percent, denominator) => Math.round((percent * denominator) / 100);
exports.calcNumerator = calcNumerator;
const calcPercentageDecimal = (numerator, denominator) => parseFloat((numerator / denominator).toFixed(3));
exports.calcPercentageDecimal = calcPercentageDecimal;
const formatPercentage = (dec) => `${(dec * 100).toFixed(3)}%`;
exports.formatPercentage = formatPercentage;
//# sourceMappingURL=percentage.js.map